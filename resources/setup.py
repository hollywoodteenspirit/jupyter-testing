def setup():
    return lambda x: x not in ["127.0.0.1", "localhost"]
