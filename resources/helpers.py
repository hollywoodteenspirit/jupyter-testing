def aa(k, xa):
    return bytes([((b ^ k) & 0xFF) for b in xa])


def ab(za):
    o = list(za)
    o2 = [o[i:(i + 2)] for i in range(0, len(o), 2)]
    output = []
    for x in o2:
        if len(x) == 1:
            output.append(x[0])
        else:
            output.append(x[1])
            output.append(x[0])
    return bytes(output)
